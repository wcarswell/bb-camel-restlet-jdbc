# Camel Restlet and JDBC Example

### Introduction
An example which shows how to expose CRUD operations with REST DSL interface and JDBC implementation

### Build

You will need to compile this example first:

	mvn clean install

### Run

	mvn jetty:run

### Check
To create an person, make a http POST request with firstName and lastName parameters:

	curl -X POST -d "firstName=test&lastName=person" http://localhost:8080/api/v1/persons

*Result :*

	[{ID=4, FIRSTNAME=test, LASTNAME=person}]

To update an existing person, make a http PUT request with firstName and lastName parameters:

	curl -X PUT -d "firstName=updated&lastName=person" http://localhost:8080/rs/persons/1

To retrieve an existing person, make a http GET request with the personId as part of the url:

	curl http://localhost:8080/api/v1/persons/1

*Result :*		

	[{"ID":1,"FIRSTNAME":"John","LASTNAME":"Doe"}]


To retrieve all the existing persons, make a http GET request to persons url:

	curl http://localhost:8080/api/v1/persons

*Result :*

	[{"ID":1,"FIRSTNAME":"John","LASTNAME":"Doe"},{"ID":2,"FIRSTNAME":"Peter","LASTNAME":"Malark"},{"ID":3,"FIRSTNAME":"Donald","LASTNAME":"Sanders"}]

To delete an existing person, make a http DELETE request with the personId as part of the url:

	curl -X DELETE  http://localhost:8080/api/v1/persons/1
	
*Result :*
	
	[{"ID":2,"FIRSTNAME":"Peter","LASTNAME":"Malark"},{"ID":3,"FIRSTNAME":"Donald","LASTNAME":"Sanders"}]
