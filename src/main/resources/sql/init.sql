CREATE TABLE person(
  id INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  firstName VARCHAR(100) DEFAULT NULL,
  lastName VARCHAR(100) DEFAULT NULL
);

INSERT INTO person (firstName, lastName) VALUES ('John','Doe');
INSERT INTO person (firstName, lastName) VALUES ('Peter','Malark');
INSERT INTO person (firstName, lastName) VALUES ('Donald','Sanders');